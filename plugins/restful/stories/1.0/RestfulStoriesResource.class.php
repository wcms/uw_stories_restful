<?php

/**
 * @file
 */

/**
 *
 */
class RestfulStoriesResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {

    // The public fields for this endpoint.
    $public_fields = parent::publicFieldsInfo();

    // Get the headline for the story.
    $public_fields['headline'] = array(
      'property' => 'title',
    );

    // Get the sub headline for the story.
    $public_fields['subhead'] = array(
      'property' => 'field_subhead',
      'sub_property' => 'value',
    );

    // Get the sub headline for the story.
    $public_fields['date'] = array(
      'property' => 'created',
      'process_callbacks' => array(
        array($this, 'dateProcess'),
      ),
    );

    // Get the sub headline for the story.
    $public_fields['author'] = array(
      'property' => 'field_story_author_name',
    );

    // Get the image for the story.
    $public_fields['image'] = array(
      'property' => 'field_photo',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
    );

    // Get the list of topics for the story (taxonomy).
    $public_fields['topics'] = array(
      'property' => 'field_topics_societal_relevance',
      'process_callbacks' => array(
        array($this, 'taxonomyMultipleProcess'),
      ),
    );

    // Get the audience for the story (taxonomy).
    $public_fields['audience'] = array(
      'property' => 'field_audience',
      'process_callbacks' => array(
        array($this, 'taxonomyMultipleProcess'),
      ),
    );

    // Get the list of faculties for the story (taxonomy).
    $public_fields['faculties'] = array(
      'property' => 'field_topics_area',
      'process_callbacks' => array(
        array($this, 'taxonomyMultipleProcess'),
      ),
    );

    // Get the type of story (taxonomy).
    $public_fields['type'] = array(
      'property' => 'field_story_type',
      'process_callbacks' => array(
        array($this, 'taxonomySingleProcess'),
      ),
    );

    // Return the list of public fields.
    return $public_fields;
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {

    // If we have a numeric array, setp through each value and output cleaned image.
    if (static::isArrayNumeric($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->imageProcess($item);
      }
      return $output;
    }

    // Add caption from image_field_caption module if present.
    $caption = (isset($value['image_field_caption']) ? $value['image_field_caption']['value'] : NULL);

    // The cleaned image array.
    return array(
      'id' => $value['fid'],
      'self' => file_create_url($value['uri']),
      'filemime' => $value['filemime'],
      'filesize' => $value['filesize'],
      'width' => $value['width'],
      'height' => $value['height'],
      'alt' => $value['alt'],
      'title' => $value['title'],
      'caption' => $caption,
    );
  }

  /**
   * Process callback, return only name and tid where there are multiple terms.
   *
   * @param object $value
   *   The taxonomny object.
   *
   * @return array
   *   A cleaned taxonomy array.
   */
  protected function taxonomyMultipleProcess($values) {

    // Step through each of the topics and setup cleaned array.
    foreach ($values as $key => $value) {
      $new_values[$key]['name'] = $value->name;
      $new_values[$key]['tid'] = $value->tid;
    }

    // The cleaned taxonomy list.
    return $new_values;
  }

  /**
   * Process callback, return only name and tid for single taxonomny term.
   *
   * @param object $value
   *   The taxonomy object.
   *
   * @return array
   *   A cleaned taxonomy array.
   */
  protected function taxonomySingleProcess($values) {

    // Just return the name and tid since there is only one term.
    $new_values['name'] = $values->name;
    $new_values['tid'] = $values->tid;

    // Return the new values.
    return $new_values;
  }

  /**
   * Process callback, return user friendly date.
   *
   * @param array $value
   *   The array of date.
   *
   * @return array
   *   A cleaned date array.
   */
  protected function dateProcess($value) {

    // Set the date on the homepage.
    $value = date('F d, Y', $value);

    // Return the new value with the user friendly date.
    return $value;
  }

}
