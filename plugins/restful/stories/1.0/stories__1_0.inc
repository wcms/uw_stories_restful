<?php

/**
 * @file
 */

$plugin = array(
  'label' => t('Stories'),
  'resource' => 'stories',
  'name' => 'stories__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_stories',
  'description' => t('A node from UW stories.'),
  'class' => 'RestfulStoriesResource',
);
